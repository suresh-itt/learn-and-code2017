
import java.util.Scanner;
import java.util.Stack;

public class FootbalFest {

    
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Stack<Integer> stack = new Stack<Integer>();

        int testCase = sc.nextInt();
        while (testCase != 0) {
            while (!stack.isEmpty()) {
                stack.pop();
            }
            int pass = sc.nextInt();
            int id = sc.nextInt();
            if (!validate(testCase, pass, id)) {
                return;
            }
            stack.push(id);
            int i = 0;
            while (i < pass) {
                char passType = sc.next().charAt(0);
                
                switch (passType) {
                    case 'B':
                        int firstTop = stack.peek();
                        stack.pop();
                        int secondTop = stack.peek();
                        stack.push(firstTop);
                        stack.push(secondTop);
                        i++;
                        break;

                    case 'P':
                        int id1 = sc.nextInt();
                        if (!validate(id1)) {
                            return;
                        }
                        stack.push(id1);
                        i++;
                        break;
                    default:
                        return;  
                }
            }
            System.out.println("Player " + stack.peek());
            testCase--;
        }

    }
    
    static boolean validate(int testcase, int pass, int id) {
        
            if (testcase >= 0 && testcase <= 100) {
                if (pass >= 1 && pass <= 100000) {
                    if (id >= 1 && id <= 1000000) {
                        return true;
                    }
                }
            }
        
       
        return false;
    }
    static boolean validate(int id1) {
        if (id1 >= 1 && id1 <= 1000000) {
            return true;
        }

        return false;

    }

}
